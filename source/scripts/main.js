var slideIndex = 0;
var slidetimeout;
var section_list = ["page-top", "section_1", "section_2", "section_3", "section_4"];
var buttons = ["welcome", "button1", "button2", "button3", "button4" ];

showSlides();
window.onscroll = function() {scrollFunction()};
navhighlight();


var modals = document.getElementsByClassName("modal");
var closes = document.getElementsByClassName("close");

var prev = document.getElementsByClassName("prev");
prev[0].addEventListener("click", function() {nextSlide(-1);});

var next = document.getElementsByClassName("next");
next[0].addEventListener("click", function() {nextSlide(1);});


for (var i = 0; i < modals.length - 1; i++) {
  (function (n) {
    document.getElementById("modal"+n).addEventListener("click", function() {
      console.log("open " + n, this.className, this.id);
      event.stopPropagation();
      document.getElementById("content" + n).style.display="block";});
  }) (i);
}

for (var i = 0; i < modals.length - 1; i++) {
  (function (n) {
    document.getElementById("close_button"+n).addEventListener("click", function() {
      console.log("close", this.className, this.id);
      event.stopPropagation();
      document.getElementById("content" + n).style.display="none";});
  }) (i);
}



function navhighlight() {
  for (var i = 0; i < buttons.length; i++) {
    (function (n) {
      document.getElementById(buttons[n]).addEventListener("click", function() {highlight(n);});
    }) (i);
  }
}

function highlight(n) {
  var target = Math.max(0, document.getElementById(section_list[n]).offsetTop - document.getElementById("navbar").offsetHeight);
  var cur = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
  var prev = cur;
  var step = 25;
  if (target < cur)
    step = -25;
  var t = window.setInterval(function(){
    var target = Math.max(0, document.getElementById(section_list[n]).offsetTop - document.getElementById("navbar").offsetHeight);
    var curY = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    if ((step > 0 && target > curY) || (step < 0 && target < curY)){
      window.scrollBy(0, step);
    }else {
      clearInterval(t);
    }
    var curY = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    if (prev == curY)
      clearInterval(t);
    if (Math.abs(step) > Math.abs(target - curY)){
      window.scrollTo(0, target);
      clearInterval(t);
    }
    prev = curY;
  }, 10);
}


function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    return (docViewBottom >= elemTop);
}

function isScrolledOutOfView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    return (docViewBottom >= elemBottom + 280);
}

var bgLoaded = false;
var divTwoLoaded = false;
var lastY = 0;
function scrollFunction() {
  var curY = $(window).scrollTop();
  if (bgLoaded && isScrolledOutOfView('#section_1_div1')) {
    bgLoaded = false;
    let sectionOne = document.getElementById('section_1_div1');
    let sectionTwo = document.getElementById('section_1_div2');
    let z = 1;
    let zz = 0;
    sectionOne.style.opacity = z;
    let t = setInterval(()=> {
      z -= 0.1;
      zz += 0.1;
      sectionOne.style.opacity = z;
      sectionTwo.style.opacity = zz;
      if (z <= 0 && zz >= 1) clearInterval(t)
    }, 150);
  }
  if (!bgLoaded && isScrolledIntoView('#section_1_div1') && !isScrolledOutOfView('#section_1_div1')) {
    bgLoaded = true;
    let sectionOne = document.getElementById('section_1_div1');
    let sectionTwo = document.getElementById('section_1_div2');
    let z = -0.4;
    let zz = 1;
    sectionOne.style.opacity = z;
    let t = setInterval(()=> {
      z += 0.1;
      zz -= 0.1;
      sectionOne.style.opacity = z;
      sectionTwo.style.opacity = zz;
      if (z >= 1 && zz <= 0) clearInterval(t)
    }, 150);
  }

  if (curY > 80){
    document.getElementsByClassName("navbar")[0].style.fontSize = "1rem";
    document.getElementsByClassName("navbar")[0].style.padding = "1.1rem";
    document.getElementById("m").style.fontSize = "0.6rem";
    document.getElementById("m").style.padding = "0.4rem";
  } else {
    document.getElementsByClassName("navbar")[0].style.fontSize = "1.5rem";
    document.getElementsByClassName("navbar")[0].style.padding = "1.5rem";
    document.getElementById("m").style.fontSize = "1rem";
    document.getElementById("m").style.padding = "0.5rem";
  }
  var highlight = -1;
  for (var i = 0; i < section_list.length; i++) {
    var target = Math.max(0, document.getElementById(section_list[i]).offsetTop-document.getElementById("navbar").offsetHeight);
    if (curY+1 >= target) {
      highlight = i;
    }
  }
  for (var i = 0; i < section_list.length; i++) {
    if (i != highlight) {
        document.getElementById(buttons[i]).style.fontSize="1rem";
    } else {
      if (!document.getElementById("menu").checked) {
        document.getElementById(buttons[i]).style.fontSize="1.5rem";
      }
    }
  }
  var elemBottom = $('#section_4').offset().top + $('#section_4').height();
  var docViewBottom = $(window).scrollTop() + $(window).height();
  if ( docViewBottom > elemBottom ) {
      document.getElementById('section_4').style.paddingTop = Math.min(650, Math.max(30, 1.18*(docViewBottom - elemBottom))) + 'px';
      document.getElementById('fixed_background').scrollTop = 1.15 * (docViewBottom - elemBottom);
  }
  lastY = curY;
}



function nextSlide(n) {
  window.clearTimeout(slidetimeout);
  if (n == 1) {
    if (slideIndex == 0) {
      slideIndex = 1;
    } else if (slideIndex < 5) {
      slideIndex = 5;
    } else {
      slideIndex = 0
    }
  } else {
    if (slideIndex == 0) {
      slideIndex = 5;
    } else if (slideIndex < 5) {
      slideIndex = 0;
    } else {
      slideIndex = 1
    }
  }
  showSlides();
}

function plusSlide() {
  slideIndex++;
  showSlides();
}

function showSlides() {
  var slides = document.getElementsByClassName("carousel");
  if (slideIndex < 0)
    slideIndex = slides.length - 1;
  if (slideIndex >= slides.length)
    slideIndex = 0;
  for (var i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex].style.display = "block";

  if (slideIndex == 0) {
    slidetimeout = window.setTimeout(plusSlide, 2400);
  } else {
    slidetimeout = window.setTimeout(plusSlide, 1300);
  }
}
